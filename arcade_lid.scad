NORMAL_BUTTON_WIDTH=30.3;
SMALL_BUTTON_WIDTH=24.5;
JOYSTICK_WIDTH=35;

LID_WIDTH=300;
LID_DEPTH=190;
LID_HEIGHT=50;
WALL_THICKNESS=8;

BOX_HEIGHT= 50;

$fn=100;

JOYSTICK_TRANSLATE = [LID_WIDTH / 4,0,0];

BUTTON_OFFSET_X = -1 * LID_WIDTH / 4;

BUTTON_DISTANCE = NORMAL_BUTTON_WIDTH / 2 + 25; 


START_BUTTON_OFFSET_X = BUTTON_DISTANCE;

START_BUTTON_OFFSET_Y = LID_DEPTH * .35;

DIST_BETWEEN_SCREWS_Y = 40;

DIST_BETWEEN_SCREWS_X = 80;

JOYSTICK_SCREW_WIDTH=4;

PEG_HEIGHT = 6.5; 

PEG_DIAMETER = 8; 

CIRCLE_PIECE_DIAMETER = (DIST_BETWEEN_SCREWS_X) * 1.75;

NUM_JOYDISC_PEGS = 8;
FASTENER_HOLE_DIST = DIST_BETWEEN_SCREWS_X * 1.5;

module bigShell() {
    difference () {
       cube([LID_WIDTH, LID_DEPTH, LID_HEIGHT], center=true);
       translate([0,0,WALL_THICKNESS]){
       cube([LID_WIDTH - WALL_THICKNESS, LID_DEPTH- WALL_THICKNESS, LID_HEIGHT], center=true);
           }
    }
}


module screwholes() {
            translate([LID_WIDTH / 4 + DIST_BETWEEN_SCREWS_X / 2,DIST_BETWEEN_SCREWS_Y / 2,0]) {
            cylinder(100, d = JOYSTICK_SCREW_WIDTH, center=true);
        }
        
            translate([LID_WIDTH / 4 - DIST_BETWEEN_SCREWS_X / 2,DIST_BETWEEN_SCREWS_Y / 2,0]) {
            cylinder(100, d = JOYSTICK_SCREW_WIDTH, center=true);
        }
        
        translate([LID_WIDTH / 4 + DIST_BETWEEN_SCREWS_X / 2,DIST_BETWEEN_SCREWS_Y / -2,0]) {
            cylinder(100, d = JOYSTICK_SCREW_WIDTH, center=true);
        }
        
            translate([LID_WIDTH / 4 - DIST_BETWEEN_SCREWS_X / 2,DIST_BETWEEN_SCREWS_Y / -2,0]) {
            cylinder(100, d = JOYSTICK_SCREW_WIDTH, center=true);
        }
        
    
    }


module mainbase() {
    difference() {
        bigShell();

  //      translate(JOYSTICK_TRANSLATE) {
  //          cylinder(100, d = JOYSTICK_WIDTH, center=true);
  //      }
        
  //      screwholes();
        translate (JOYSTICK_TRANSLATE) {
            cylinder(100, d=CIRCLE_PIECE_DIAMETER * .7, center=true);
            
            
            
  //    for (i = [1:1:NUM_JOYDISC_PEGS]) {
//        rotate([0,0,(360 / NUM_JOYDISC_PEGS) * i]){
//            translate([CIRCLE_PIECE_DIAMETER/2 -  PEG_DIAMETER/2, 0,WALL_THICKNESS/2]){
//                cylinder(PEG_HEIGHT, d=PEG_DIAMETER*.85, $fn=100); 
//            }
//        }
//    }
            
            
//                      for (i = [1:1:NUM_JOYDISC_PEGS]) {
//        rotate([0,0,(360 / NUM_JOYDISC_PEGS) * i]){
//            translate([FASTENER_HOLE_DIST/2 -  PEG_DIAMETER/2, 0, 0 -3]){
//                cylinder(PEG_HEIGHT, d=PEG_DIAMETER*.85, $fn=100); 
//            }
//        }
//    }
            
            
            
            for (i = [1:1:NUM_JOYDISC_PEGS]) {
                rotate([0,0,(360 / NUM_JOYDISC_PEGS) * i]){
                    translate([FASTENER_HOLE_DIST/2 -  PEG_DIAMETER/2, 0,WALL_THICKNESS/2]){
                        cylinder(100, d=PEG_DIAMETER, $fn=100, center=true); 
                    }
                }
            }
        }
        
        

            
        
  
        
        
        rotate([0,0,0]) {
        // regular buttons top row
        translate([BUTTON_OFFSET_X, BUTTON_DISTANCE/2, 0]) {
            cylinder(100, d = NORMAL_BUTTON_WIDTH, center=true);
        }
        translate([BUTTON_OFFSET_X + BUTTON_DISTANCE, BUTTON_DISTANCE/2, 0]) {
            cylinder(100, d = NORMAL_BUTTON_WIDTH, center=true);
        }
        translate([BUTTON_OFFSET_X - BUTTON_DISTANCE, BUTTON_DISTANCE/2, 0]) {
            cylinder(100, d = NORMAL_BUTTON_WIDTH, center=true);
        }

        // regular buttons bottom row
        translate([BUTTON_OFFSET_X, -1 * BUTTON_DISTANCE/2, 0]) {
            cylinder(100, d = NORMAL_BUTTON_WIDTH, center=true);
        }
        translate([BUTTON_OFFSET_X + BUTTON_DISTANCE, -1 * BUTTON_DISTANCE/2, 0]) {
            cylinder(100, d = NORMAL_BUTTON_WIDTH, center=true);
        }
        translate([BUTTON_OFFSET_X - BUTTON_DISTANCE, -1 * BUTTON_DISTANCE/2, 0]) {
            cylinder(100, d = NORMAL_BUTTON_WIDTH, center=true);
        }
    }

        // start select button
        translate([LID_WIDTH/-4,0,0]) {
        translate([START_BUTTON_OFFSET_X/2,START_BUTTON_OFFSET_Y, 0]) {
            cylinder(100, d = SMALL_BUTTON_WIDTH, center=true);
        }
        
            translate([-START_BUTTON_OFFSET_X/2,START_BUTTON_OFFSET_Y, 0]) {
            cylinder(100, d = SMALL_BUTTON_WIDTH, center=true);
        }
    }
        

    }
}

module pegs(thickness_multiplier = 1) {
    translate([LID_WIDTH/2 - WALL_THICKNESS/2, 0, LID_HEIGHT/2 + PEG_HEIGHT/2]) {
        cylinder(PEG_HEIGHT, d=PEG_DIAMETER * thickness_multiplier, center=true);
    }

    translate([LID_WIDTH/2 - WALL_THICKNESS/2, LID_DEPTH/2 - WALL_THICKNESS/2, LID_HEIGHT/2 + PEG_HEIGHT/2]) {
        cylinder(PEG_HEIGHT, d=PEG_DIAMETER * thickness_multiplier, center=true);
    }

    translate([LID_WIDTH/2 - WALL_THICKNESS/2, -1 * (LID_DEPTH/2 - WALL_THICKNESS/2), LID_HEIGHT/2 + PEG_HEIGHT/2]) {
        cylinder(PEG_HEIGHT, d=PEG_DIAMETER * thickness_multiplier, center=true);
    }


    translate([-1 * (LID_WIDTH/2 - WALL_THICKNESS/2), 0, LID_HEIGHT/2 + PEG_HEIGHT/2]) {
        cylinder(PEG_HEIGHT, d=PEG_DIAMETER * thickness_multiplier,  center=true);
    }

    translate([0, LID_DEPTH/2 - WALL_THICKNESS/2, LID_HEIGHT/2 + PEG_HEIGHT/2]) {
        cylinder(PEG_HEIGHT, d=PEG_DIAMETER * thickness_multiplier,  center=true);
    }

    translate([0, -1 * (LID_DEPTH/2 - WALL_THICKNESS/2), LID_HEIGHT/2 + PEG_HEIGHT/2]) {
        cylinder(PEG_HEIGHT, d=PEG_DIAMETER * thickness_multiplier,  center=true);
    }
    
    
        translate([LID_WIDTH/-2 + WALL_THICKNESS/2, LID_DEPTH/2 - WALL_THICKNESS/2, LID_HEIGHT/2 + PEG_HEIGHT/2]) {
        cylinder(PEG_HEIGHT, d=PEG_DIAMETER * thickness_multiplier,  center=true);
    }

        translate([LID_WIDTH/-2 + WALL_THICKNESS/2, -1 * (LID_DEPTH/2 - WALL_THICKNESS/2), LID_HEIGHT/2 + PEG_HEIGHT/2]) {
        cylinder(PEG_HEIGHT, d=PEG_DIAMETER * thickness_multiplier,  center=true);
    }
    
}

//pegs();
mainbase();



module joydisc() {
    difference() {
        cylinder(WALL_THICKNESS, d = CIRCLE_PIECE_DIAMETER, center=true );
        translate([DIST_BETWEEN_SCREWS_X / 2,DIST_BETWEEN_SCREWS_Y / 2,0]) {
            cylinder(100, d = JOYSTICK_SCREW_WIDTH, center=true);
        }
        
        translate([ DIST_BETWEEN_SCREWS_X / -2,DIST_BETWEEN_SCREWS_Y / 2,0]) {
            cylinder(100, d = JOYSTICK_SCREW_WIDTH, center=true);
        }
        
        translate([DIST_BETWEEN_SCREWS_X / 2,DIST_BETWEEN_SCREWS_Y / -2,0]) {
            cylinder(100, d = JOYSTICK_SCREW_WIDTH, center=true);
        }
        
            translate([ DIST_BETWEEN_SCREWS_X / -2,DIST_BETWEEN_SCREWS_Y / -2,0]) {
            cylinder(100, d = JOYSTICK_SCREW_WIDTH, center=true);
        }
        
        cylinder(1000, d=JOYSTICK_WIDTH,center=true);
        
           for (i = [1:1:NUM_JOYDISC_PEGS]) {
        rotate([0,0,(360 / NUM_JOYDISC_PEGS) * i]){
            translate([FASTENER_HOLE_DIST/2 -  PEG_DIAMETER/2, 0, 0 -3]){
                cylinder(PEG_HEIGHT, d=PEG_DIAMETER*.85, $fn=100); 
            }
        }
    }
    }
    
 
}



//joydisc();

//LID_WIDTH=254;
//LID_DEPTH=200;
//LID_HEIGHT=25;

CHARGER_HEIGHT = 10; 
CHARGER_WIDTH = 10;
DISTANCE_BETWEEN_CHARGER_AND_POWER=69;
DISTANCE_BETWEEN_HOLES_SMALL= 21.95;
DISTANCE_BETWEEN_HOLES_LONG = 92;
DISTANCE_BETWEEN_HOLES_EDGE_LONG_SIDE = .5; 
DISTANCE_BETWEEN_HOLES_EDGE_SHORT_SIDE = 2; 
BATTERY_HOLE_DIAMETER = 1.8;
BATTERY_PEG_HEIGHT = 10; 
DISTANCE_BETWEEN_CHARGER_AND_HOLE=3; 

DISTANCE_BETWEEN_CHARGER_AND_EDGE = 10; 

EDGE_BUFFER = 2; 




module boxcontainer() {
    difference() {
        cube([LID_WIDTH, LID_DEPTH, BOX_HEIGHT], center=true);
        translate([0,0,LID_HEIGHT/-2  + BOX_HEIGHT/2  - PEG_HEIGHT ]) //
        {pegs(1.00);}
        translate([0,0,WALL_THICKNESS]) {
            cube([LID_WIDTH - 2 * WALL_THICKNESS, LID_DEPTH - 2 * WALL_THICKNESS, BOX_HEIGHT], center=true);
        }
        translate([0,DISTANCE_BETWEEN_HOLES_LONG /-2 + DISTANCE_BETWEEN_CHARGER_AND_HOLE + CHARGER_WIDTH/2, -1 * BOX_HEIGHT/2  + WALL_THICKNESS]) {
            cube([10 + LID_WIDTH, CHARGER_WIDTH, CHARGER_HEIGHT]);
        }
        translate([0,DISTANCE_BETWEEN_HOLES_LONG /-2 + DISTANCE_BETWEEN_CHARGER_AND_HOLE + DISTANCE_BETWEEN_CHARGER_AND_POWER + CHARGER_WIDTH/2, -1 * BOX_HEIGHT/2 + WALL_THICKNESS]) {
            cube([10 + LID_WIDTH, CHARGER_WIDTH, CHARGER_HEIGHT]);
        }

    }
    translate([LID_WIDTH/2 -  WALL_THICKNESS - DISTANCE_BETWEEN_HOLES_EDGE_LONG_SIDE -   EDGE_BUFFER, DISTANCE_BETWEEN_HOLES_LONG /2 + BATTERY_HOLE_DIAMETER/2,  BOX_HEIGHT/-2 + WALL_THICKNESS/2 + BATTERY_PEG_HEIGHT/2 ]) {
       cylinder(BATTERY_PEG_HEIGHT,d=BATTERY_HOLE_DIAMETER, center=true);
    }
    translate([LID_WIDTH/2 -  WALL_THICKNESS - DISTANCE_BETWEEN_HOLES_EDGE_LONG_SIDE   - EDGE_BUFFER, DISTANCE_BETWEEN_HOLES_LONG /-2 - BATTERY_HOLE_DIAMETER/2 ,  BOX_HEIGHT/-2 + WALL_THICKNESS/2 + BATTERY_PEG_HEIGHT/2 ]) {
       cylinder(BATTERY_PEG_HEIGHT,d=BATTERY_HOLE_DIAMETER, center=true);
    }
    
    translate([LID_WIDTH/2 -  WALL_THICKNESS - DISTANCE_BETWEEN_HOLES_EDGE_LONG_SIDE - DISTANCE_BETWEEN_HOLES_SMALL  - EDGE_BUFFER, DISTANCE_BETWEEN_HOLES_LONG /2 + BATTERY_HOLE_DIAMETER/2, BOX_HEIGHT/-2 + WALL_THICKNESS/2 + BATTERY_PEG_HEIGHT/2 ]) {
       cylinder(BATTERY_PEG_HEIGHT,d=BATTERY_HOLE_DIAMETER, center=true);
    }
    translate([LID_WIDTH/2 -  WALL_THICKNESS - DISTANCE_BETWEEN_HOLES_EDGE_LONG_SIDE - DISTANCE_BETWEEN_HOLES_SMALL - EDGE_BUFFER, DISTANCE_BETWEEN_HOLES_LONG /-2 - BATTERY_HOLE_DIAMETER/2, BOX_HEIGHT/-2 + WALL_THICKNESS/2 + BATTERY_PEG_HEIGHT/2]) {
       cylinder(BATTERY_PEG_HEIGHT,d=BATTERY_HOLE_DIAMETER, center=true);
    }
}

//        translate([0,0,LID_HEIGHT - PEG_HEIGHT - PEG_HEIGHT - PEG_HEIGHT]) //
//        {pegs(1.00);}

//boxcontainer();

