#define led 2
#define BUTTON1 27
#define BUTTON2 25
#define BUTTON3 32
#define BUTTON4 22
#define BUTTON5 21
#define BUTTON6 17

#define JOYUP 4
#define JOYDOWN 0
#define JOYRIGHT 2
#define JOYLEFT 16

#define PLAYER1BUTTON 26
#define PLAYER2BUTTON 18

#include <BleGamepad.h> 

BleGamepad bleGamepad("Arcade Stick!", "Living Room");



void setup() {
    //Serial.begin(9600);
    pinMode(led, OUTPUT); 
     pinMode(BUTTON1, INPUT_PULLUP);
     pinMode(BUTTON2, INPUT_PULLUP);
     pinMode(BUTTON3, INPUT_PULLUP);
     pinMode(BUTTON4, INPUT_PULLUP);
     pinMode(BUTTON5, INPUT_PULLUP);
     pinMode(BUTTON6, INPUT_PULLUP);
     
     pinMode(JOYUP, INPUT_PULLUP);
     pinMode(JOYDOWN, INPUT_PULLUP);
     pinMode(JOYRIGHT, INPUT_PULLUP);     
     pinMode(JOYLEFT, INPUT_PULLUP);
      
     pinMode(PLAYER1BUTTON, INPUT_PULLUP); 
     pinMode(PLAYER2BUTTON, INPUT_PULLUP); 
     bleGamepad.begin();
}


void pollButton(int buttonStatus, int gamepadButton) {
  
  if (buttonStatus == 0 ){
      bleGamepad.release(gamepadButton);
      //Serial.println("released button");
  } else if (buttonStatus == 1 && bleGamepad.isPressed(gamepadButton) == 0) {
          bleGamepad.press(gamepadButton); 
        
    
  }
      // do nothing, the button is already pressed and we don't wanna release it. 
}

void loop() {

    int button1 = !digitalRead(BUTTON1);
    int button2 = !digitalRead(BUTTON2);
    int button3 = !digitalRead(BUTTON3);
    int button4 = !digitalRead(BUTTON4);
    int button5 = !digitalRead(BUTTON5);
    int button6 = !digitalRead(BUTTON6);

    int joyUp = !digitalRead(JOYUP); 
    int joyDown = !digitalRead(JOYDOWN); 
    int joyRight = !digitalRead(JOYRIGHT); 
    int joyLeft = !digitalRead(JOYLEFT); 

    int player1Button = !digitalRead(PLAYER1BUTTON); 
    int player2Button = !digitalRead(PLAYER2BUTTON); 

    if (bleGamepad.isConnected()){
      //Serial.println("somehow connected to shit"); 
      // Underscores are the libraries, not mine.

      pollButton(button1, BUTTON_8); 
      pollButton(button2, BUTTON_9); 
      pollButton(button3, BUTTON_10); 
      pollButton(button4, BUTTON_11);
      pollButton(button5, BUTTON_12);
      pollButton(button6, BUTTON_13);

      pollButton(joyUp, DPAD_UP); 
      pollButton(joyDown, DPAD_DOWN);
      pollButton(joyLeft, DPAD_LEFT); 
      pollButton(joyRight, DPAD_RIGHT); 
      
      pollButton(player1Button, BUTTON_14); 
      pollButton(player2Button, BUTTON_15); 

    }


}
