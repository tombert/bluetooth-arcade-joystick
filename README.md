# Arcade Bluetooth Controller

A DIY Bluetooth arcade controller.

## Why?

Because it was (at least at the time) difficult to find an inexpensive bluetooth controller with real arcade parts.  I thought this was stupid, because genuine arcade parts are not expensive, and neither are bluetooth-capable controllers like the esp32.  

## What do you need (physically)? 

- A 3D printer.  You'll need a relatively large one to use the unmodified models, as one of the dimensions is 290mm. 
- An esp-wroom-32. I used [this](https://www.amazon.com/ACEIRMC-ESP-WROOM-32-Bluetooth-Development-Compatible/dp/B08PNWB81Z/ref=sr_1_6?crid=37TYJW5LFOGKT&keywords=esp-wroom-32&qid=1698301293&sprefix=esp-wroo%2Caps%2C73&sr=8-6) one.
- Arcade parts.  It might be easiest to buy one of those DIY arcade stick kits on Amazon (like [this](https://www.ebay.com/itm/292730909621?hash=item44281f33b5:g:t3kAAOSwM~RbnyCc&amdata=enc%3AAQAIAAAA8L%2BLjYVnH8Ri44CxSN8isYbX3mdK6yNJhBmWBw3UOGXj9ubCW5l8BSc8hMMAazHiYUDQAkczXW6WtPn8z2vgr2iz66NRKSlRKVLXE5TTO8Nu4h5SM%2F5EXreMcR7A7oJGtek2KlC%2BwBl%2BzO3mNGZSGD%2Bf385df3ivP6ndVqtKQC2kdrfBE35c7hxaqv1GS2aDMDUtaB4KIkimwmZKJo%2FbybjOQRLdLit8gb1YcYgdCeaS8aHdORH03wbn6CB7%2BVyfz27ONawxvgw1SK1Mlo42RwIUus1k1iGS7ljcnxY%2FP1OPoYZAeE4vyjrogu%2F2w2c65A%3D%3D%7Ctkp%3ABk9SR9bot6rtYg))
    - You will need to adjust the numbers in the SCAD file to make sure that the holes for it are the right size. 
- A caliper.  You'll probably need to measure and fine-tune the widths and the like for your specific buttons.  
- Blank circuit boards. Not strictly necessary but it's nice since you'll need a lot of ground spots and it's easier to unify them if you have a board to mount stuff on. 
- Some kind of power bank.  A cell phone power bank can work, but the provided models are designed around a [cheaper one off Banggood](https://usa.banggood.com/Geekcreit-ESP32-ESP32S-18650-Battery-Charge-Shield-V3-Micro-USB-Type-A-USB-0_5A-Test-Charging-Protection-Board-p-1265088.html?cur_warehouse=CN&rmmds=search). You just need something that can easily output 5V DC. 
- Hinges. Find something cheap to screw the lid and bottom together. 
- A latch, to keep the housing together but also make it easy to open up. 
- Lots and lots of jumpers. 
- Weights.  Not strictly necessary but makes it feel nicer.  Just some beanbags or something will work. 

## What do you need (software)? 

- The Arduino IDE. 
- The [ESP32 BLE Gamepad library](https://github.com/lemmingDev/ESP32-BLE-Gamepad)
- OpenSCAD (to make modifications to the models). 
